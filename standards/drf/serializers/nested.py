from typing import Dict

from django.contrib.contenttypes.models import ContentType

from rest_framework import serializers

from .utils import get_relation_key

__all__ = ('NestedListSerializer', 'NestedSerializerMixin')


"""
    Usage example:

    class SomeNestedSerializer(serializers.ModelSerializer):
        ...
        _delete = serializers.BooleanField(required=False, write_only=True)

        class Meta:
            ...
            extra_kwargs = {'id': {'read_only': False, 'required': False}}
            fields = ('id', '_delete', ...)
            list_serializer_class = NestedListSerializer
            ...

    class SomeM2MNestedSerializer(serializers.ModelSerializer):
        ...
        _delete = serializers.BooleanField(required=False, write_only=True)

        class Meta:
            ...
            extra_kwargs = {'id': {'read_only': False, 'required': False}}
            fields = ('id', '_delete', ...)
            list_serializer_class = NestedListSerializer
            ...


    class SomeGenericNestedSerializer(serializers.ModelSerializer):
        ...
        _delete = serializers.BooleanField(required=False, write_only=True)

        class Meta:
            ...
            extra_kwargs = {'id': {'read_only': False, 'required': False}}
            fields = ('id', '_delete', ...)
            list_serializer_class = NestedListSerializer
            ...


    class SomeParentSerializer(
        NestedSerializerMixin,
        serializers.ModelSerializer
    ):
        nested_conf = {
            "some_field1": {"related_name": "some_name1"},
            "some_field2": {
                "object_id_field": "object_id",
                "content_type_field": "content_type",
            },
        }
        ...
        some_field1 = SomeNestedSerializer(many=True)
        some_field2 = SomeM2MNestedSerializer(many=True)
        some_field3 = SomeGenericNestedSerializer(many=True)
        ...

        def create(self, data):
            some_field1 = data.pop("some_field1", [])
            some_field2 = data.pop("some_field2", [])
            some_field3 = data.pop("some_field3", [])
            self.instance = super().create(data)
            self.update_nested('some_field1', some_field1)
            self.update_m2m_nested('some_field2', some_field2)
            self.update_generic_nested('some_field3', some_field3)
            return self.instance

        def update(self, instance, data):
            some_field1 = data.pop("some_field1", [])
            some_field2 = data.pop("some_field2", [])
            some_field3 = data.pop("some_field3", [])
            self.instance = super().update(instance, data)
            self.update_nested('some_field1', some_field1)
            self.update_m2m_nested('some_field2', some_field2)
            self.update_generic_nested('some_field3', some_field3)
            return self.instance
"""


class NestedSerializerMixin:
    nested_conf = {
        # "some_nested_field": {"related_name": "some_name"},
        # "some_generic_field": {
        #     "object_id_field": "object_id",  # not requider
        #     "content_type_field": "content_type",  # not requider
        # },
    }

    def get_nested_query(self, instance: 'Model', field: str) -> 'Queryset':
        method_name = f'get_nested_{field}_query'
        method = getattr(self, method_name, None)
        if method:
            return method(instance)
        return getattr(instance, field).all()

    def get_related_name(self, field: str) -> str:
        nested_name = self.nested_conf.get(field, {}).get('related_name')
        if not nested_name:
            serializer_field = self.fields[field]
            nested_name = get_relation_key(
                related_name=field,
                child_model=(
                    serializer_field.child.Meta.model
                    if getattr(serializer_field, 'many', False)
                    else serializer_field.Meta.model
                ),
                parent_model=self.Meta.model
            )
        return nested_name

    def create_single_nested(self, field: str, data: Dict) -> 'Model':
        data[field] = self.fields[field].create(data.pop(field, {}))
        return data[field]

    def update_nested(self, field: str, data: Dict) -> None:
        if not data:
            return

        nested_name = self.get_related_name(field)
        self.fields[field].partial = self.partial
        self.fields[field].update(
            self.get_nested_query(self.instance, field),
            [
                {nested_name: self.instance, **item}
                for item in data
            ]
        )

    def update_m2m_nested(self, field: str, data: Dict) -> None:
        if not data:
            return

        self.fields[field].partial = self.partial
        result = self.fields[field].update(
            getattr(self.instance, field).model._default_manager.all(),
            data
        )
        to_add = []
        for item in result:
            if item.get('to_delete'):
                getattr(self.instance, field).remove(item.get('id'))
            else:
                to_add.append(item.get('obj'))
        getattr(self.instance, field).add(*to_add)

    def update_generic_nested(self, field: str, data: Dict) -> None:
        if not data:
            return

        ct = ContentType.objects.get_for_model(self.instance)
        field_conf = self.nested_conf.get(field, {})
        id_field = field_conf.get("object_id_field", "object_id")
        ct_field = self.nested_conf.get("content_type_field", "content_type")
        self.fields[field].partial = self.partial
        self.fields[field].update(
            self.get_nested_query(self.instance, field),
            [
                {id_field: self.instance.id, ct_field: ct, **item}
                for item in data
            ]
        )


class NestedListSerializer(serializers.ListSerializer):
    delete_key = '_delete'

    def update(self, objects: 'Queryset', data: Dict):
        obj_map = {obj.id: obj for obj in objects}

        result = []
        for item in data:
            obj_id = item.pop('id', None)
            to_delete = item.pop(self.delete_key, False)
            obj = obj_map.get(obj_id)

            if obj:
                if to_delete:
                    result.append({
                        'id': obj_id,
                        'obj': None,
                        'to_delete': to_delete,
                    })
                    destroy = getattr(self.child, 'perform_destroy', None)
                    if destroy:
                        destroy(obj)
                    else:
                        obj.delete()
                else:
                    result.append({
                        'id': obj_id,
                        'obj': self.child.update(obj, item),
                        'to_delete': to_delete,
                    })
            elif not to_delete:
                result.append({
                    'id': None,
                    'obj': self.child.create(item),
                    'to_delete': to_delete,
                })
        return result
