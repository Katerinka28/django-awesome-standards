from django import forms

from django_filters.widgets import RangeWidget

__all__ = ('StandardRangeWidget', )


class StandardRangeWidget(RangeWidget):

    def value_from_datadict(self, data, files, name):
        res = []
        for i, widget in enumerate(self.widgets):
            value = widget.value_from_datadict(data, files, name)
            if value is not None:
                limits = str(value).split('-')
                value = (limits[i] if len(limits) > i else None)
            res.append(value or None)
        return res
