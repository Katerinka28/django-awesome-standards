from django import template

from django_jinja import library
import jinja2

from standards.frendly_url.services import frendlyurl_service

__all__ = ('frendly_abs_url', 'frendly_url', 'get_frendly_url')

register = template.Library()


@register.simple_tag
def get_frendly_url(*args, **kwargs):
    return frendlyurl_service.get_url(*args, **kwargs)


@library.global_function
@jinja2.pass_context
def frendly_url(context, *args, **kwargs):
    kwargs.setdefault('view_kwargs', context['view'].kwargs)
    return frendlyurl_service.get_url(*args, **kwargs)


@library.global_function
@jinja2.pass_context
def frendly_abs_url(context, *args, **kwargs):
    return (
        context['request']
        .build_absolute_uri(
            frendly_url(context, *args, **kwargs)
        )
    )


@library.global_function
@jinja2.pass_context
def frendly_modify_url(context, *args, **kwargs):
    return (
        frendlyurl_service
        .modify_reversed_url(context['request'].path, **kwargs)
    )


@library.global_function
@jinja2.pass_context
def frendly_abs_modify_url(context, *args, **kwargs):
    return (
        context['request']
        .build_absolute_uri(
            frendly_modify_url(context, *args, **kwargs)
        )
    )
