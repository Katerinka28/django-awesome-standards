from copy import deepcopy
from typing import Any, Dict, List, Union

from django.urls import resolve, reverse
from django.utils.datastructures import MultiValueDict

from standards.const import settings

__all__ = ('FrendlyUrlService', 'frendlyurl_service')


class FrendlyUrlService:
    """
    Provides encode, decode and modify frendly params.
    """

    # Encode
    def _join_keys(self, data: List) -> str:
        return ';'.join(data)

    def _join_group(self, key: str, values: str) -> str:
        return f'{key}={values}'

    def _join_values(self, data: Union[List, str]) -> str:
        if hasattr(data, '__iter__') and not isinstance(data, str):
            return ','.join([str(item) for item in data])
        return data

    def encode(self, params: Dict) -> str:
        return self._join_keys([
            self._join_group(key, self._join_values(params.getlist(key)))
            for key in sorted(params)
        ])

    # Decode
    def _split_keys(self, line: str) -> List:
        return line.split(';')

    def _split_group(self, line: str) -> List:
        data = line.partition('=')
        return data[0], data[2]

    def _split_values(self, line: str) -> List:
        return line.split(',')

    def decode(self, line: str) -> MultiValueDict:
        """
        param "line": /attribute=long-value,short;price-range=400-900/
        """
        data = MultiValueDict()
        if not line:
            return data

        for item in self._split_keys(line):
            key, values = self._split_group(item)
            data.setlist(key, self._split_values(values))
        return data

    def replace_param(
        self,
        data: MultiValueDict,
        param: str,
        value: Any
    ) -> MultiValueDict:
        data.setlist(param, [value])
        return data

    def remove_param(
        self,
        data: MultiValueDict,
        param: str
    ) -> MultiValueDict:
        data.pop(param, None)
        return data

    def get_url(
        self,
        url_name: str,
        view_kwargs: Dict = {},
        page: int = None,
        **kwargs
    ) -> str:
        """
        Return reversed url with modified frendly params by url_name.
        """
        params = deepcopy(view_kwargs)
        page = page or int(view_kwargs.get(settings.PAGE_PARAM, 1))
        if page and page > 1:
            params[settings.PAGE_PARAM] = page

        filters = view_kwargs.get(settings.FILTERS_PARAM)
        data = (
            self.decode(filters)
            if filters
            else MultiValueDict()
        )
        for k in kwargs:
            data.setlist(
                k,
                kwargs[k] if isinstance(kwargs[k], list) else [kwargs[k]]
            )
        if data:
            params[settings.FILTERS_PARAM] = self.encode(data)

        return reverse(url_name, kwargs=params)

    def modify_reversed_url(self, url: str, **kwargs):
        """
        Return reversed url with modified frendly params based on another url.
        """
        try:
            match = resolve(url)
        except Exception:
            pass
        else:
            url_name = (
                f"{match.namespace}:{match.url_name}"
                if match.namespace
                else match.url_name
            )
            try:
                return (
                    self
                    .get_url(
                        url_name,
                        view_kwargs=match.kwargs,
                        **kwargs
                    )
                )
            except Exception:
                pass
        return url


frendlyurl_service = FrendlyUrlService()
