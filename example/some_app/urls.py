from django.conf.urls import url

from standards.frendly_url.urls import frendly_path, frendly_re_path

from .views import *

urlpatterns = [
    url(r'^$', SomePageView.as_view(), name='some_page'),
    url(r'^redirect/$', RedirectView.as_view(), name='some_redirect'),

    url(r'^api/v1/some-api/$', SomeAPIView.as_view(), name='some_api'),
    frendly_re_path(
        r'^api/v1/user/list/$',
        UserListAPIView.as_view(),
        name='user_list'
    ),
    # frendly_path(
    #     'api/v1/user/list/',
    #     UserListAPIView.as_view(),
    #     name='user_list'
    # ),
    url(
        r'^api/v1/user/(?P<pk>\d+)/update/$',
        UserRetrieveUpdateAPIView.as_view(),
        name='user_update'
    ),
]
