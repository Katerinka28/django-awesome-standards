from collections import OrderedDict

from .base import ModelSerializer

__all__ = ('EntitySerializerMixin', 'EntityModelSerializer')


class EntitySerializerMixin:

    def to_representation(self, instance):
        return OrderedDict([
            ("id", instance.id),
            ("caption", str(instance)),
            ("type", instance.__class__.__name__),
            ("props", super().to_representation(instance)),
        ])


class EntityModelSerializer(EntitySerializerMixin, ModelSerializer):
    pass
