from rest_framework import serializers

__all__ = ('StandardSerializerMixin', 'Serializer', 'ModelSerializer')


class StandardSerializerMixin:

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = self.context.get('request')


class Serializer(StandardSerializerMixin, serializers.Serializer):
    pass


class ModelSerializer(StandardSerializerMixin, serializers.ModelSerializer):
    pass