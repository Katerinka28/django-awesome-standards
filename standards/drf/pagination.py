from base64 import b64decode, b64encode
from collections import OrderedDict
from typing import Dict
from urllib import parse

from django.core.paginator import InvalidPage

from rest_framework import pagination
from rest_framework.exceptions import NotFound
from rest_framework.pagination import (
    Cursor,
    CursorPagination as BaseCursorPagination,
    _positive_int,
)
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param

from standards.const import settings

__all__ = (
    'CursorPagination',
    'cursor_pagination',
    'LimitOffsetPagination',
    'limitoffset_pagination',
    'PageNumberPagination',
    'pagenumber_pagination',
)


class StandardPaginationMixin:

    def paginate_queryset(
        self,
        queryset,
        request,
        view=None,
        query_params: Dict = None
    ):
        self.set_query_params(request, view, query_params)
        return super().paginate_queryset(queryset, request, view)

    def get_query_params(
        self,
        request,
        view=None,
        query_params: Dict=None
    ) -> OrderedDict:
        if query_params:
            return query_params

        if hasattr(view, 'get_pagination_params'):
            return view.get_pagination_params()

        return getattr(
            view,
            'query_params',
            getattr(
                request,
                'query_params',
                getattr(request, 'GET', {})
            )
        )

    def set_query_params(
        self,
        request,
        view=None,
        query_params: Dict=None
    ) -> OrderedDict:
        self.query_params = self.get_query_params(
            request=request,
            view=view,
            query_params=query_params
        )
        return self.query_params

    def get_pagination_info(self, data) -> Dict:
        raise NotImplementedError('Method "get_pagination_info" not implemented')

    def get_paginated_response(self, data) -> Response:
        return Response(OrderedDict([
            ('items', data),
            ('pagination', self.get_pagination_info(data)),
        ]))

    def get_results(self, data) -> Dict:
        return data.get('items')


class PageNumberPagination(
    StandardPaginationMixin,
    pagination.PageNumberPagination
):
    page_query_param = settings.PAGE_PARAM

    def get_pagination_info(self, data):
        return {
            'count': self.page.paginator.count,
            'page_size': self.page_size,

            'previous_page': (
                self.page.previous_page_number()
                if self.page.has_previous()
                else None
            ),
            'curent_page': self.page.number,
            'next_page': (
                self.page.next_page_number()
                if self.page.has_next()
                else None
            ),
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
        }

    def paginate_queryset(
        self,
        queryset,
        request,
        view=None,
        query_params: Dict = None
    ):
        """
        Paginate a queryset if required, either returning a
        page object, or `None` if pagination is not configured for this view.
        """
        self.set_query_params(request, view, query_params)
        page_size = self.get_page_size(request)
        if not page_size:
            return None

        paginator = self.django_paginator_class(queryset, page_size)
        page_number = self.query_params.get(self.page_query_param, 1)
        if page_number in self.last_page_strings:
            page_number = paginator.num_pages

        try:
            self.page = paginator.page(page_number)
        except InvalidPage as exc:
            msg = self.invalid_page_message.format(
                page_number=page_number, message=str(exc)
            )
            raise NotFound(msg)

        if paginator.num_pages > 1 and self.template is not None:
            # The browsable API should display pagination controls.
            self.display_page_controls = True

        self.request = request
        return list(self.page)

    def get_page_size(self, request):
        if self.page_size_query_param:
            try:
                return _positive_int(
                    self.query_params[self.page_size_query_param],
                    strict=True,
                    cutoff=self.max_page_size
                )
            except (KeyError, ValueError):
                pass

        return self.page_size


def pagenumber_pagination(
    page_size: int,
    page_query_param: str = None,
    **kwargs
):
    class Klass(PageNumberPagination):
        def __init__(self, *base_args, **base_kwargs):
            super().__init__(*base_args, **base_kwargs)
            self.page_size = page_size
            if page_query_param:
                self.page_query_param = page_query_param
            for key in kwargs:
                setattr(self, key, kwargs[key])
    return Klass


class LimitOffsetPagination(
    StandardPaginationMixin,
    pagination.LimitOffsetPagination
):

    def get_pagination_info(self, data):
        return  {
            'limit': self.limit,
            'offset': self.offset,
            'total': self.count,
        }

    def paginate_queryset(
        self,
        queryset,
        request,
        view=None,
        query_params: Dict = None
    ):
        self.set_query_params(request, view, query_params)
        self.count = self.get_count(queryset)
        self.limit = self.get_limit(request)
        self.offset = self.get_offset(request)
        self.request = request

        if not self.limit:
            return list(queryset[self.offset:])

        if self.count > self.limit and self.template is not None:
            self.display_page_controls = True

        if self.count == 0 or self.offset > self.count:
            return []
        return list(queryset[self.offset:self.offset + self.limit])

    def get_limit(self, request):
        if self.limit_query_param:
            try:
                return _positive_int(
                    self.query_params[self.limit_query_param],
                    strict=True,
                    cutoff=self.max_limit
                )
            except (KeyError, ValueError):
                pass

        return self.default_limit

    def get_offset(self, request):
        try:
            return _positive_int(
                self.query_params[self.offset_query_param],
            )
        except (KeyError, ValueError):
            return 0


def limitoffset_pagination(default_limit=None, max_limit=None, **kwargs):
    class Klass(LimitOffsetPagination):
        def __init__(self, *base_args, **base_kwargs):
            super().__init__(*base_args, **base_kwargs)
            self.max_limit = max_limit
            if default_limit:
                self.default_limit = default_limit
            for key in kwargs:
                setattr(self, key, kwargs[key])
    return Klass


class CursorPagination(
    StandardPaginationMixin,
    pagination.CursorPagination
):
    ordering = '-id'

    def paginate_queryset(
        self,
        queryset,
        request,
        view=None,
        query_params: Dict = None
    ):
        self.count = self.get_count(queryset)
        return super().paginate_queryset(queryset, request, view, query_params)

    def get_ordering(self, request, queryset, view):
        filter_cls = getattr(view, 'filter_class', None)
        if filter_cls:
            query_params = getattr(
                view,
                'query_params',
                getattr(
                    request,
                    'query_params',
                    getattr(request, 'GET', {})
                )
            )
            filterset = filter_cls(
                data=query_params,
                queryset=queryset,
                request=request,
            )
            try:
                from django_filters.filters import OrderingFilter
                filterset.form.is_valid()
                query_data = filterset.form.cleaned_data
                for name, item in filterset.filters.items():
                    if isinstance(item, OrderingFilter):
                        return [
                            item.get_ordering_value(param)
                            for param in query_data.get(name)
                        ]
            except Exception:
                pass
        return super().get_ordering(request, queryset, view)

    def get_pagination_info(self, data):
        return  {
            'count': self.count,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'page_size': self.page_size,
        }

    def get_count(self, queryset):
        """
        Determine an object count, supporting either querysets or regular lists.
        """
        try:
            return queryset.count()
        except (AttributeError, TypeError):
            return len(queryset)

    def get_page_size(self, request):
        if self.page_size_query_param:
            try:
                return _positive_int(
                    self.query_params[self.page_size_query_param],
                    strict=True,
                    cutoff=self.max_page_size
                )
            except (KeyError, ValueError):
                pass
        return self.page_size

    def prepare_cursor(self, cursor):
        tokens = {}
        if cursor.offset != 0:
            tokens['o'] = str(cursor.offset)
        if cursor.reverse:
            tokens['r'] = '1'
        if cursor.position is not None:
            tokens['p'] = cursor.position

        querystring = parse.urlencode(tokens, doseq=True)
        return b64encode(querystring.encode('ascii')).decode('ascii')

    def encode_cursor(self, cursor):
        return replace_query_param(
            self.base_url,
            self.cursor_query_param,
            self.prepare_cursor(cursor)
        )

    def decode_cursor(self, request):
        encoded = self.query_params.get(self.cursor_query_param)
        if encoded is None:
            return None

        try:
            querystring = b64decode(encoded.encode('ascii')).decode('ascii')
            tokens = parse.parse_qs(querystring, keep_blank_values=True)

            offset = tokens.get('o', ['0'])[0]
            offset = _positive_int(offset, cutoff=self.offset_cutoff)

            reverse = tokens.get('r', ['0'])[0]
            reverse = bool(int(reverse))

            position = tokens.get('p', [None])[0]
        except (TypeError, ValueError):
            raise NotFound(self.invalid_cursor_message)
        return Cursor(offset=offset, reverse=reverse, position=position)

    def get_next_link(self):
        if not self.has_next:
            return None

        if self.page and self.cursor and self.cursor.reverse and self.cursor.offset != 0:
            # If we're reversing direction and we have an offset cursor
            # then we cannot use the first position we find as a marker.
            compare = self._get_position_from_instance(self.page[-1], self.ordering)
        else:
            compare = self.next_position
        offset = 0

        has_item_with_unique_position = False
        for item in reversed(self.page):
            position = self._get_position_from_instance(item, self.ordering)
            if position != compare:
                # The item in this position and the item following it
                # have different positions. We can use this position as
                # our marker.
                has_item_with_unique_position = True
                break

            # The item in this position has the same position as the item
            # following it, we can't use it as a marker position, so increment
            # the offset and keep seeking to the previous item.
            compare = position
            offset += 1

        if self.page and not has_item_with_unique_position:
            # There were no unique positions in the page.
            if not self.has_previous:
                # We are on the first page.
                # Our cursor will have an offset equal to the page size,
                # but no position to filter against yet.
                offset = self.page_size
                position = None
            elif self.cursor and self.cursor.reverse:
                # The change in direction will introduce a paging artifact,
                # where we end up skipping forward a few extra items.
                offset = 0
                position = self.previous_position
            elif self.cursor:
                # Use the position from the existing cursor and increment
                # it's offset by the page size.
                offset = self.cursor.offset + self.page_size
                position = self.previous_position

        if not self.page:
            position = self.next_position

        cursor = Cursor(offset=offset, reverse=False, position=position)
        return self.encode_cursor(cursor)


def cursor_pagination(page_size: int = None, **kwargs):
    class Klass(CursorPagination):
        def __init__(self, *base_args, **base_kwargs):
            super().__init__(*base_args, **base_kwargs)
            if page_size:
                self.page_size = page_size
            for key in kwargs:
                setattr(self, key, kwargs[key])
    return Klass