from django_filters import RangeFilter

from .fields import StandardRangeField

__all__ = ('StandardRangeFilter', )


class StandardRangeFilter(RangeFilter):
    field_class = StandardRangeField
