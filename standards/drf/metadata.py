from collections import OrderedDict
from typing import Dict

from django.http.response import Http404
from django.utils.encoding import force_text
from django.urls import reverse

from rest_framework import exceptions, serializers
from rest_framework.metadata import SimpleMetadata
from rest_framework.request import clone_request

__all__ = ('FieldsetMetadata', )


class FieldsetMetadata(SimpleMetadata):
    """
    It returns an ad-hoc set of information about the view.
    Includes filter meta, view extra meta and extended field choices.

    1) Filter metadata available if view has attribute "filterset_class".
    Example:
    ````
    class SomeView(...):
        filterset_class = SomeFilterset
    ```
    To exclude choices from filters with many values use:
    - get_exclude_filter_choice_meta view method
    or
    - exclude_filter_choice_meta view attribute.

    2) View extra metadata available if view has method "get_extra_meta".
    Example:
    ```
    class SomeView(...):

        def get_extra_meta(self) -> Optional[Dict, List]:
            return {
                'some_field': SomeModelSerializer(
                    SomeModel.objects.all(), many=True
                ).data
            }
    ```

    3) View field choices metadata available if view has method "get_meta_choices".
    Example:
    ```
    class SomeView(...):

        def get_meta_choices(self) -> Dict:
            return {
                'some_field': {
                    'some_key1': 'some_field'               # will get object attribute
                    'some_key2': 'get_some_param'           # will call object method
                    'some_key3': lambda obj: obj.some_attr  # will call lambda with object param
                    'some_key4': self.get_some(obj)         # will call some metod with object param
                }
            }
    ```

    4) To insert some data into filter:
    Example:
    ```
    class SomeFilterSet(...):

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            ...
            self.filters[key].extra['initial'] = {'min': 1, 'max': 10}
    ```
    """
    available_actions = ('GET', 'PATCH', 'POST', 'PUT')

    def determine_extra(self, request, view):
        return view.get_extra_meta()

    def determine_filter_choices(
        self,
        request,
        view,
        filter_type,
        filter_name
    ) -> Dict:
        method = getattr(view, 'get_exclude_filter_choice_meta', None)
        choice_exclude = (
            method()
            if method
            else getattr(view, 'exclude_filter_choice_meta', [])
        )
        if filter_name in choice_exclude:
            return {}

        url_name = (
            filter_type
            .extra
            .get(
                'choice_url_name',
                getattr(filter_type, 'choice_url_name', None)
            )
        )
        if url_name:
            try:
                return {
                    'choice_url': (
                        request
                        .build_absolute_uri(reverse(url_name))
                    )
                }
            except:
                pass

        choices = (
            filter_type
            .extra
            .get(
                'choices',
                getattr(filter_type, 'choices', None)
            )
        )
        if not choices is None:
            return {
                'choices': [
                    {
                        'value': choice_value,
                        'label': force_text(choice_name)
                    }
                    for choice_value, choice_name in choices
                ]
            }

        choice_query = (
            filter_type
            .extra
            .get(
                'queryset',
                getattr(filter_type, 'queryset', None)
            )
        )
        if choice_query:
            attr_name = filter_type.extra.get('to_field_name', 'id')
            return {
                'choices': [
                    {
                        'value': getattr(item, attr_name),
                        'label': force_text(str(item))
                    }
                    for item in choice_query
                ]
            }

        return {}

    def determine_filters(self, request, view):
        filters = OrderedDict()
        queryset = view.get_queryset()
        if not queryset:
            return filters

        filterset = getattr(view, 'filterset', None)
        if not filterset and view.filter_class:
            filterset = view.filter_class(
                data=request.GET,
                request=request,
                queryset=queryset,
            )

        if not filterset:
            return filters

        for filter_name, filter_type in filterset.filters.items():
            attrs = OrderedDict()
            attrs['url_key'] = filter_name
            attrs['label'] = filter_type.label
            attrs['type'] = filter_type.__class__.__name__
            attrs.update(
                self.determine_filter_choices(
                    request,
                    view,
                    filter_type,
                    filter_name,
                )
            )

            initial = filter_type.extra.get('initial')
            if not initial is None:
                attrs['initial'] = initial

            filters[filter_name] = attrs
        return filters

    def determine_metadata(self, request, view):
        self.view = view
        metadata = super().determine_metadata(request, view)

        if hasattr(view, 'filter_class'):
            metadata['filters'] = self.determine_filters(request, view)

        if hasattr(view, 'get_extra_meta'):
            metadata['extra_meta'] = self.determine_extra(request, view)
        return metadata

    def determine_actions(self, request, view):
        actions = {}
        for method in set(self.available_actions) & set(view.allowed_methods):
            view.request = clone_request(request, method)
            try:
                if hasattr(view, 'check_permissions'):
                    view.check_permissions(view.request)
                if method == 'PUT' and hasattr(view, 'get_object'):
                    view.get_object()
            except (exceptions.APIException, exceptions.PermissionDenied, Http404):
                pass
            else:
                serializer = view.get_serializer()
                if not serializer:
                    continue

                actions[method] = self.get_serializer_info(serializer)
            finally:
                view.request = request
        return actions

    def get_field_info(self, field):
        field_info = super().get_field_info(field)
        if field_info.get('read_only'):
            return field_info

        if hasattr(field, 'choices'):
            field_info['choices'] = [
                {
                    'value': choice_value,
                    'label': force_text(choice_name),
                }
                for choice_value, choice_name in field.choices.items()
            ]

        if hasattr(field, 'queryset'):
            extra_params = (
                getattr(self.view, 'extra_meta_choices', {})
                .get(field.source, {})
            )

            def get_item_extra(item):
                extra = {}
                for name, key in extra_params.items():
                    value = getattr(item, key, None)
                    extra[name] = value() if callable(value) else value
                return extra

            queryset = getattr(field, 'queryset', [])
            if not hasattr(queryset, '__iter__'):
                queryset = queryset.all()

            field_info['choices'] = [
                {
                    'value': item.id,
                    'label': force_text(str(item)),
                    **get_item_extra(item)
                }
                for item in queryset
            ]
        return field_info
