from enum import Enum

from rest_framework.settings import (
    APISettings,
    DEFAULTS as BASE_DEFAULTS,
    IMPORT_STRINGS as BASE_IMPORT_STRINGS,
)

__all__ = ('VIEW_SCOPES', 'settings')


class VIEW_SCOPES(Enum):
    generic = 1
    create = 2
    list = 3
    receive = 4
    remove = 5
    update = 6


DEFAULTS = dict(BASE_DEFAULTS)
DEFAULTS.update({
    "PAGE_PARAM": "page",
    "FILTERS_PARAM": "filters",
})
settings = APISettings(None, DEFAULTS, BASE_IMPORT_STRINGS)
