__all__ = ('get_relation_key', )


def get_relation_key(
    related_name: str,
    parent_model: 'Model',
    child_model: 'Model',
):
    for field in child_model._meta.fields:
        if (
            field.remote_field
            and field.remote_field.model == parent_model
            and field.remote_field.related_name == related_name
        ):
            return field.name
