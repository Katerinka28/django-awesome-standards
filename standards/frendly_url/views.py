from standards.common.views import (
    FilteredViewMixin,
    QueryParamsViewMixin,
    StandardViewMixin,
)
from .services import frendlyurl_service

__all__ = (
    'FilteredFrendlyViewMixin',
    'FrendlyUrlViewMixin',
    'StandardFrendlyViewMixin',
)


class FrendlyUrlViewMixin(QueryParamsViewMixin):
    frendly_url_kwarg = 'filters'
    pagination_url_keys = (
        'page',
        'limit',
        'offset',
        'cursor',
        'reverse',
        'position',
    )

    def get_query_params(self):
        return frendlyurl_service.decode(
            self.kwargs.get(self.frendly_url_kwarg)
        )

    def get_pagination_params(self):
        data = {}
        for name in self.pagination_url_keys:
            param = (
                self.kwargs.get(name)
                or self.query_params.get(name)
                or self.base_params.get(name)
            )
            if param:
                data[name] = param
        return data


class FilteredFrendlyViewMixin(FrendlyUrlViewMixin, FilteredViewMixin):
    pass



class StandardFrendlyViewMixin(FrendlyUrlViewMixin, StandardViewMixin):
    pass
