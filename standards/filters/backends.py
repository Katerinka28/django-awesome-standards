from django_filters.rest_framework import backends
from rest_framework.filters import SearchFilter

__all__ = ('DjangoFilterBackend', 'SearchFilterBackend')


class DjangoFilterBackend(backends.DjangoFilterBackend):

    def get_filterset_kwargs(self, request, queryset, view):
        return {
            # 'data': getattr(view, 'pagination_params', request.query_params),
            'data': getattr(view, 'query_params', request.query_params),
            'queryset': queryset,
            'request': request,
        }


class SearchFilterBackend(SearchFilter):

    def filter_queryset(self, request, queryset, view):
        self.view = view
        return super().filter_queryset(request, queryset, view)

    def to_html(self, request, queryset, view):
        self.view = view
        return super().to_html(request, queryset, view)

    def get_search_terms(self, request):
        """
        Search terms are set by a ?search=... query parameter,
        and may be comma and/or whitespace delimited.
        """
        params = (
            (
                getattr(self.view, 'query_params', {})
                .get(self.search_param)
            )
            or (
                getattr(self.view, 'base_query_params', {})
                .get(self.search_param)
            )
            or request.query_params.get(self.search_param, '')
        )
        params = params.replace('\x00', '')  # strip null characters
        params = params.replace(',', ' ')
        return params.split()