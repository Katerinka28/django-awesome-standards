from django_filters.fields import RangeField

from .widgets import StandardRangeWidget

__all__ = ('StandardRangeField', )


class StandardRangeField(RangeField):
    widget = StandardRangeWidget
