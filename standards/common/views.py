from typing import Dict

__all__ = ('QueryParamsViewMixin', 'FilteredViewMixin')


class QueryParamsViewMixin:

    @property
    def base_params(self):
        if not hasattr(self, '_base_params'):
            self._base_params = self.get_base_params()
        return self._base_params

    @property
    def query_params(self):
        if not hasattr(self, '_query_params'):
            self._query_params = self.get_query_params()
        return self._query_params

    def get_base_params(self):
        return getattr(
            self.request,
            'GET',
            getattr(self.request, 'query_params', {})
        )

    def get_query_params(self):
        return self.get_base_params()

    def get_pagination_param(self, name):
        return self.query_params.get(name, self.base_params.get(name))

    def get_pagination_params(self):
        data = {
            'cursor': self.get_pagination_param('cursor'),
            'limit': self.get_pagination_param('limit'),
            'offset': self.get_pagination_param('offset'),
            'order': self.get_pagination_param('order'),
            'page': self.get_pagination_param('page'),
        }
        return {key: data[key] for key in data if data[key] is not None}


class FilteredViewMixin(QueryParamsViewMixin):
    filter_class = None

    def get_filter_kwargs(self, qs: 'Queryset') -> Dict:
        return {
            'data': self.query_params,
            'queryset': qs,
        }

    def get_base_queryset(self):
        return super().get_queryset()

    def get_queryset(self):
        base_qs = self.get_base_queryset()
        if self.filter_class is None:
            return base_qs

        self.filterset = self.filter_class(
            **self.get_filter_kwargs(base_qs)
        )
        return self.filterset.qs


class PaginationViewMixin(QueryParamsViewMixin):
    """
    Required to use standard DRF paginators
    """
    pagination_class = None

    def get_pagination_class(self):
        return self.pagination_class

    @property
    def paginator(self):
        """
        The paginator instance associated with the view, or `None`.
        """
        if not hasattr(self, '_paginator'):
            pagination_class = self.get_pagination_class()
            if pagination_class is None:
                self._paginator = None
            else:
                self._paginator = pagination_class()
        return self._paginator

    def paginate_queryset(self, queryset):
        if self.paginator is None:
            return None

        return (
            self
            .paginator
            .paginate_queryset(queryset, self.request, view=self)
        )

    def get_pagination_context(self, object_list=None):
        qs = object_list if object_list is not None else self.object_list
        paginated_qs = self.paginate_queryset(qs)
        if paginated_qs is None:
            return {
                'paginator': None,
                'pagination_info': None,
                'object_list': qs,
                'queryset': qs,
            }
        return {
            'paginator': self.paginator,
            'pagination_info': (
                self
                .paginator
                .get_pagination_info(paginated_qs)
            ),
            'object_list': paginated_qs,
            'queryset': qs,
        }

    def get_context_data(self, *, object_list=None, **kwargs):
        context = self.get_pagination_context(object_list)
        if self.context_object_name is not None:
            context[self.context_object_name] = context['object_list']
        context.update(kwargs)
        return super().get_context_data(**context)


class StandardViewMixin(FilteredViewMixin, PaginationViewMixin):
    pass
