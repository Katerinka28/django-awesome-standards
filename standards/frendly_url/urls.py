from django.urls import path, re_path, include

from standards.const import settings

__all__ =('frendly_path', 'frendly_re_path')

path_filter_part = f'<path:{settings.FILTERS_PARAM}>/'
path_page_part = f'page/<int:{settings.PAGE_PARAM}>/'
re_path_filter_part = f'(?P<{settings.FILTERS_PARAM}>[^/]+=[^/]+)/'
re_path_page_part = f'page/(?P<{settings.PAGE_PARAM}>\d+)/'


def join(path: str, has_end: bool = False) -> str:
    return f'{path}{"$" if has_end else ""}'


def frendly_re_path(regex, view, kwargs=None, name=None):
    clean_regex = str(regex)
    has_end = regex.endswith('$')
    if has_end:
        clean_regex = clean_regex[:-1]

    return re_path(
        clean_regex,
        include(
            [
                re_path(url, view, kwargs=kwargs, name=name)
                for url in [
                    join(f"^{re_path_filter_part}{re_path_page_part}", has_end),
                    join(f"^{re_path_page_part}", has_end),
                    join(f"^{re_path_filter_part}", has_end),
                    join("^", has_end),
                ]
            ]
        )
    )


def frendly_path(route, view, kwargs=None, name=None):
    return path(route, include(([
        path(
            f"{path_filter_part}{path_page_part}",
            view,
            kwargs=kwargs,
            name=name
        ),
        path(path_page_part, view, kwargs=kwargs, name=name),
        path(path_filter_part, view, kwargs=kwargs, name=name),
        path('', view, kwargs=kwargs, name=name),
    ])))
